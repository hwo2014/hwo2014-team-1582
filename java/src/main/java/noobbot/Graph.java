package noobbot;

import com.sun.org.apache.bcel.internal.generic.AALOAD;
import java.awt.List;
import java.util.ArrayList;

public class Graph {
    ArrayList < Object > adj[];
    int N;
    boolean Visited[];
    
    public Graph(int N){
        this.N = N;
        adj = new ArrayList[N];
        for(int i = 0; i < N; i++)
            adj[i] = new ArrayList<>();
        Visited = new boolean[N];
        
    }
    
    public void unir(int a, int b){
        adj[a].add(b);
    }
    
}
