package noobbot;

public class JoinRace extends SendMsg{
    BotId botId;
    String trackName;
    int carCount;
    
    public JoinRace(final String name,final String key,final String track,final int cc) {
        botId = new BotId(name, key);
        trackName = track;
        carCount = cc;
    }
    
    @Override
    protected String msgType() {
        return "joinRace";
    }
    
    @Override
    protected Object msgData() {
        return this;
    }
}
