package noobbot;

import com.google.gson.Gson;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.Socket;

public class Main {
    public static void main(String[] args) throws IOException {
        // CLAVE: X9UP8Fhnkh   testserver.helloworldopen.com 8091 ACM1PT IQGsrXOpCgRYhA
        String host = args[0];
        //String host = "testserver.helloworldopen.com";
        int port = Integer.parseInt(args[1]);
        //int port = 8091;
        String botName = args[2];
        //String botName = "ACM1PT";
        String botKey = args[3];
        //String botKey = "IQGsrXOpCgRYhA";
        System.out.println("Connecting to " + host + ":" + port + " as " + botName + "/" + botKey);
        
        final Socket socket = new Socket(host, port);
        final PrintWriter writer = new PrintWriter(new OutputStreamWriter(socket.getOutputStream(), "utf-8"));
        final BufferedReader reader = new BufferedReader(new InputStreamReader(socket.getInputStream(), "utf-8"));
        
        String trackU = "usa";
        int carCount = 1;
        new Main(reader, writer, new JoinRace(botName, botKey, trackU, carCount));
    }

    final Gson gson = new Gson();
    private PrintWriter writer;
    

    public Main(final BufferedReader reader, final PrintWriter writer, final JoinRace join) throws IOException {
        this.writer = writer;
        String line = null;
        send(join);
        double trottle = 1;              // ACELERACION
        boolean crashed = false;
        boolean gameStarted = false;
        
        //DATOS
        double angle;
        double inpiecedistance;
        int carril = 0;
        int numpiece = 0;
        int pieceAnterior = 0;
        Car mycar = null;
        int carIndex = -1;
        double velocidad = 0, deltavel = 0, velant = 0;
        double pxAnterior = 0;
        int tickant = 0;
        boolean turboAvailable = false;
        
        while( (line = reader.readLine()) != null ) {
            final MsgWrapper msgFS = gson.fromJson(line, MsgWrapper.class);
            
            if( msgFS.msgType.equals("carPositions") ){         //CAR POSITIONS
                final CarPositions cP = gson.fromJson(line, CarPositions.class);
                
                System.out.println("Tick --> " + cP.gameTick);
                
                String msgFSString = "{cars=" + cP.data.toString() + "}";
                msgFSString = msgFSString.replaceAll("switch", "Switch");
                CarPositionsData cpd = gson.fromJson(msgFSString, CarPositionsData.class);
                
                if( carIndex == -1 || !cpd.cars[carIndex].id.equals(mycar) ){
                    carIndex = 0;
                    for(CarPositionsDataElement it: cpd.cars){
                        if( it.id.equals(mycar) ) break;
                        carIndex++;
                    }
                }
                CarPositionsDataElement info = cpd.cars[carIndex];
                angle = info.angle;
                numpiece = (int) info.piecePosition.pieceIndex;
                inpiecedistance = info.piecePosition.inPieceDistance;
                int aux = (int) info.piecePosition.lane.startLaneIndex;
                if( carriles[carril].index != aux )
                    for(int i = 0; i < ncarriles; i++)
                        if( carriles[i].index == aux ){
                            carril = i;
                            break;
                        }
                
                if( numpiece == pieceAnterior ) {
                    velocidad = inpiecedistance - pxAnterior;
                    velocidad /= (cP.gameTick - tickant);
                    deltavel = velocidad - velant;
                    velant = velocidad;
                } else {
                    velocidad += deltavel*(cP.gameTick - tickant);
                }
                
                pxAnterior = inpiecedistance;
                tickant = cP.gameTick;
                pieceAnterior = numpiece;
                
                if( !gameStarted ){
                    send(new Ping());
                    System.out.println(" Estate --> Stopped");
                    continue;
                }
                
                if( crashed ){
                    send(new Ping());
                    System.out.println(" Estate --> Crashed");
                    continue;
                }
                
                aux = (numpiece+1)%npieces;
                if( pista[aux].Switch && !VIS[aux] && BEST[aux][carril] != 0 ){
                    send(new SwitchLane(BEST[aux][carril]));
                    VIS[aux] = true;
                    continue;
                }
                
                if( turboAvailable && PET[numpiece] ){
                    turboAvailable = false;
                    send(new Turbo());
                    continue;
                }
                
                if( pista[numpiece].isCurva() ){
                    double r = pista[numpiece].radius - Math.signum(pista[numpiece].angle)*carriles[carril].distanceFromCenter;
                    double xd = (velocidad*velocidad/r - 0.2*Math.sin(Math.abs(angle)*Math.PI/180.0))/Math.cos(Math.abs(angle)*Math.PI/180.0);
                    double falta = (r*Math.abs(pista[numpiece].angle)*Math.PI)/180.0 - inpiecedistance;
                    if( Math.signum(angle)*Math.signum(pista[numpiece].angle) >= 0 && Math.abs(xd) < XD && velocidad < VM[aux][carril] + falta*K ) trottle = 1;
                    else if( velocidad < 4 ) trottle = 1;
                    else trottle = 0.01;
                    System.out.print("XD: " + xd + "   vel: ");
                } else {
                    double falta = pista[numpiece].length - inpiecedistance;
                    if( velocidad >= VM[aux][carril] + falta*K ) trottle = 0.01;
                    else trottle = 1;
                }
                
                System.out.println(velocidad  + " " + numpiece + " " + trottle);
                
                send(new Throttle(trottle));
                
            } else if ( msgFS.msgType.equals("join")){          //JOIN
                System.out.println("Joined");
                send(new Ping());
            } else if ( msgFS.msgType.equals("gameInit")) {     //GAME INIT
                System.out.println("Race init");
                String msgFSString = msgFS.data.toString();
                msgFSString = msgFSString.replaceAll("switch", "Switch");
                System.out.println(msgFSString);
                GameInitData gid = gson.fromJson(msgFSString, GameInitData.class);
                pista = gid.race.track.pieces;
                carriles = gid.race.track.lanes;
                
                int i = 0;
                for(Cars it: gid.race.cars){
                    if( it.id.equals(mycar) ){
                        carIndex = i;
                        break;
                    }
                    i++;
                }
                
                calcularRutaoptima();
                calcularVelLim();
                
                send(new Ping());
            } else if ( msgFS.msgType.equals("gameStart")) {    //GAME START
                System.out.println("Race start");
                gameStarted = true;
                
                send(new Ping());
            } else if( msgFS.msgType.equals("yourCar") ){       //YOUR CAR
                System.out.println("Descripcion del carro");
                mycar = gson.fromJson(msgFS.data.toString(), YourCarData.class);
                System.out.println("CAR: " + mycar.name + " COLOR: " + mycar.color);
                
                send(new Ping());
            } else if( msgFS.msgType.equals("gameEnd") ){       // GAME END
                System.out.println("Race end");
                gameStarted = false;
                GameEndData ged = gson.fromJson(msgFS.data.toString(), GameEndData.class);
                
                send(new Ping());
                System.out.println("\n\nRESULTS:");
                for(BestLaps it: ged.bestLaps){
                    System.out.println(it.car.name + ":" + it.car.color);
                }
                
            } else if( msgFS.msgType.equals("tournamentEnd") ){ //TOURNAMENT END
                System.out.println("Fin del torneo");
                
                send(new Ping());
            } else if( msgFS.msgType.equals("crash") ){         //CRASH
                System.out.println("Crashed");
                crashed = true;
                
                send(new Ping());
            } else if( msgFS.msgType.equals("spawn") ){         //SPAWN
                System.out.println("Restaurado despues de un accidente");
                crashed = false;
                
                send(new Ping());
            } else if( msgFS.msgType.equals("lapFinished") ){   //LAP FINISHED
                System.out.println("Vuelta completada");
                for(int i = 0; i < npieces; i++)
                    VIS[i] = false;
                send(new Ping());
            } else if( msgFS.msgType.equals("dnf") ){           //DNF
                System.out.println("Descalicado");
                gameStarted = false;
                
                send(new Ping());
            } else if( msgFS.msgType.equals("finish") ){        //FINISH
                System.out.println("Fin");
                
                send(new Ping());
            } else if( msgFS.msgType.equals("turboAvailable") ){//TURBO AVAILABLE
                System.out.println("Turbo Available");
                turboAvailable = true;
                turbo = gson.fromJson(msgFS.data.toString(), TurboAvailable.class);
                System.out.println(turbo.turboFactor + " --> " + turbo.turboDurationTicks);
                
                calcularMomentoTurbo();
                
                send(new Ping());
            } else {
                System.out.println("Mensaje desconocido: " + msgFS.msgType);
                System.out.println(msgFS.data);
                
                send(new Ping());
            }
        }
    }
    
    private void send(final SendMsg msg) {
        writer.println(msg.toJson());
        writer.flush();
    }
    
    Piece pista[] = null;
    Lanes carriles[] = null;
    int BEST[][], ncarriles, npieces;
    double DIST[][];
    boolean VIS[];
    
    void calcularRutaoptima(){
        int nc = carriles.length;
        int np = pista.length;
        for(int i = 0; i < nc; i++)
            for(int j = i+1; j < nc; j++)
                if( carriles[i].distanceFromCenter > carriles[j].distanceFromCenter ) swap(carriles[i],carriles[j]);
        
        VIS = new boolean[np];
        for(int i = 0; i < np; i++)
            VIS[i] = false;
        
        ncarriles = nc;
        npieces = np;
        BEST = new int[np][nc];
        DIST = new double[np][nc];
        for(int i = 0; i < nc; i++)
            DIST[0][i] = 0;
        
        for(int i = np-1; i >= 0; i-- ){
            for(int j = 0; j < nc; j++){
                DIST[i][j] = DIST[(i+1)%np][j];
                BEST[i][j] = 0;
                if( pista[i].isRecta() ) DIST[i][j] += pista[i].length;
                else DIST[i][j] += ((pista[i].radius - Math.signum(pista[i].angle)*carriles[j].distanceFromCenter)*Math.abs(pista[i].angle)*Math.PI)/180.0;
            }
            if( pista[i].Switch ){
                for(int j = 0; j < nc; j++){
                    double aux = 0;
                    if( pista[i].isRecta() ){
                        if( j > 0 ){
                            aux = DIST[(i+1)%np][j-1];
                            aux += Math.sqrt( Math.pow(pista[i].length,2) + Math.pow(carriles[j].distanceFromCenter - carriles[j-1].distanceFromCenter,2) );
                            DIST[i][j] = Math.min(DIST[i][j], aux);
                        }
                        if( j + 1 < nc ){
                            aux = DIST[(i+1)%np][j+1];
                            aux += Math.sqrt( Math.pow(pista[i].length,2) + Math.pow(carriles[j].distanceFromCenter - carriles[j+1].distanceFromCenter,2) );
                            DIST[i][j] = Math.min(DIST[i][j], aux);
                        }
                    } else {
                        if( j > 0 ){
                            aux = DIST[(i+1)%np][j-1];
                            double sign = Math.signum(pista[i].angle);
                            aux += getDistance(pista[i].radius - sign*carriles[j].distanceFromCenter, pista[i].radius - sign*carriles[j-1].distanceFromCenter, pista[i].angle);
                            DIST[i][j] = Math.min(DIST[i][j], aux);
                        }
                        if( j + 1 < nc ){
                            aux = DIST[(i+1)%np][j+1];
                            double sign = Math.signum(pista[i].angle);
                            aux += getDistance(pista[i].radius - sign*carriles[j].distanceFromCenter, pista[i].radius - sign*carriles[j+1].distanceFromCenter, pista[i].angle);
                            DIST[i][j] = Math.min(DIST[i][j], aux);
                        }
                    }
                }
            }
        }
        for(int i = 0; i < np; i++){
            for(int j = 0; j < nc; j++)
                BEST[i][j] = 0;
            if( pista[i].Switch ){
                for(int j = 0; j < nc; j++){
                    if( j > 0 && DIST[(i+1)%np][j-1] < DIST[(i+1)%np][j] ) BEST[i][j] = -1;
                    if( j + 1 < nc && DIST[(i+1)%np][j+1] < DIST[(i+1)%np][j+BEST[i][j]] ) BEST[i][j] = 1;
                }
            }
        }
    }
    
    void swap(Object a, Object b){
        Object aux = a;
        a = b;
        b = aux;
    }
    
    double getDistance(double r1, double r2, double alfa){
        alfa *= Math.PI/180.0;
        alfa = Math.abs(alfa);
        if( r1 > r2 ) {
            double aux = r1;
            r1 = r2;
            r2 = aux;
        }
        double alfa1 = Math.acos(r1/r2);
        if( alfa1 > alfa ) return Math.sqrt(r1*r1 + r2*r2 - r1*r2*Math.cos(alfa));
        double d = Math.sqrt( r2*r2 - r1*r1);
        return d + (alfa - alfa1)*r1;
    }
    
    double MAXR[];
    boolean PET[];
    TurboAvailable turbo = null;
    
    private void calcularMomentoTurbo() {
        MAXR = new double[npieces];
        MAXR[0] = 0;
        PET = new boolean[npieces];
        for(int i = 0; i < npieces; i++)
            PET[i] = false;
        for(int i = npieces - 1; i >= 0; i--)
            if( pista[i].isRecta() ) MAXR[i] = pista[i].length + MAXR[(i+1)%npieces];
            else MAXR[i] = 0;
        
        for(int i = npieces - 1; i >= 0; i--)
            if( pista[i].isRecta() ) MAXR[i] = pista[i].length + MAXR[(i+1)%npieces];
            else MAXR[i] = 0;
        
        int id = 0;
        for(int i = 1; i < npieces; i++)
            if( MAXR[i] > MAXR[id] ) id = i;
        
        PET[id] = true;
    }
    
    double aL = 0.5;
    double XD = 0.45;
    double K = 0.02;
    double VM[][];
    
    private void calcularVelLim(){
        VM = new double[npieces][ncarriles];
        for(int i = npieces-1; i >= 0; i--)
            for(int j = 0; j < ncarriles; j++){
                if( pista[i].isCurva() ) VM[i][j] = Math.sqrt(aL*(pista[i].radius - Math.signum(pista[i].angle)*carriles[j].distanceFromCenter));
                else {
                    int naux = (i+1)%npieces;
                    double dist = 0, x = pista[i].length, y = carriles[j+BEST[i][j]].distanceFromCenter - carriles[j].distanceFromCenter;
                    if( BEST[i][j] != 0 ) dist = Math.sqrt(x*x + y*y);
                    else dist = x;
                    VM[i][j] = VM[naux][j + BEST[i][j]] + K*dist;
                }
            }
        
        for(int i = npieces-1; i >= 0; i--)
            for(int j = 0; j < ncarriles; j++){
                if( pista[i].isCurva() ) VM[i][j] = Math.sqrt(aL*(pista[i].radius - Math.signum(pista[i].angle)*carriles[j].distanceFromCenter));
                else {
                    int naux = (i+1)%npieces;
                    double dist = 0, x = pista[i].length, y = carriles[j+BEST[i][j]].distanceFromCenter - carriles[j].distanceFromCenter;
                    if( BEST[i][j] != 0 ) dist = Math.sqrt(x*x + y*y);
                    else dist = x;
                    VM[i][j] = VM[naux][j + BEST[i][j]] + K*dist;
                }
            }
    }
}
