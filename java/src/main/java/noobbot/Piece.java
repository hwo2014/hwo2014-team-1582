package noobbot;

class Piece {
    double length;
    boolean Switch;
    boolean bridge;
    double radius;
    double angle;
    
    boolean isRecta(){ return radius == 0; }
    boolean isCurva(){ return length == 0; }

    public boolean isSwitch() {
        return Switch;
    }
}
