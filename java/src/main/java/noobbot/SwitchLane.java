package noobbot;

public class SwitchLane extends SendMsg{
    public String dir;

    public SwitchLane(int d) {
        dir = d==-1?"Left":"Right";
    }
    
    @Override
    protected String msgType() {
        return "switchLane";
    }
    
    @Override
    protected Object msgData() {
        return dir;
    }
}
