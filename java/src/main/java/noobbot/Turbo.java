package noobbot;

public class Turbo extends SendMsg{
    String data;
    
    public Turbo() {
        data = "pow, pow, pow -->  LIGER ZERO";
    }
    
    @Override
    protected String msgType() {
        return "turbo";
    }
    
    @Override
    protected Object msgData() {
        return data;
    }
}
