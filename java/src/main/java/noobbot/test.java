package noobbot;

import com.google.gson.Gson;

public class test {
    
    public static void main(String[] args) {
        Gson gson = new Gson();
        String msg = "{race={track={id=france, name=France, pieces=[{length=102.0}, {length=94.0}, {radius=200.0, angle=22.5, Switch=true}, {radius=200.0, angle=22.5}, {radius=200.0, angle=22.5}, {radius=200.0, angle=22.5}, {radius=100.0, angle=45.0}, {radius=100.0, angle=45.0, Switch=true}, {length=53.0}, {radius=200.0, angle=-22.5}, {radius=50.0, angle=-45.0}, {radius=100.0, angle=45.0}, {radius=100.0, angle=45.0}, {radius=100.0, angle=45.0}, {radius=50.0, angle=-45.0}, {radius=50.0, angle=-45.0}, {radius=50.0, angle=-45.0}, {radius=200.0, angle=-22.5, Switch=true}, {length=94.0}, {length=94.0}, {radius=50.0, angle=45.0}, {radius=50.0, angle=45.0}, {radius=50.0, angle=45.0}, {radius=50.0, angle=45.0}, {length=99.0, Switch=true}, {length=99.0}, {radius=100.0, angle=-45.0}, {radius=100.0, angle=-45.0}, {radius=100.0, angle=-45.0}, {radius=100.0, angle=45.0}, {radius=200.0, angle=22.5, Switch=true}, {radius=200.0, angle=22.5}, {length=53.0}, {radius=200.0, angle=22.5}, {radius=200.0, angle=22.5}, {radius=100.0, angle=45.0}, {radius=200.0, angle=22.5}, {radius=200.0, angle=22.5}, {length=94.0, Switch=true}, {length=94.0}, {length=94.0}, {length=94.0}, {length=94.0}, {length=91.0}], lanes=[{distanceFromCenter=-10.0, index=0.0}, {distanceFromCenter=10.0, index=1.0}], startingPoint={position={x=-303.0, y=-168.0}, angle=-90.0}}, cars=[{id={name=ACM1PT, color=red}, dimensions={length=40.0, width=20.0, guideFlagPosition=10.0}}], raceSession={laps=3.0, maxLapTimeMs=60000.0, quickRace=true}}}";
        GameInitData gid = gson.fromJson(msg, GameInitData.class);
        new test(gid);
    }
    
    public test(GameInitData gid){
        pista = gid.race.track.pieces;
        carriles = gid.race.track.lanes;
        int np = pista.length;
        int nc = carriles.length;
        
        for(int i = 0; i < nc; i++)
            System.out.println("Carril " + i + ": " + carriles[i].index + ", " +  carriles[i].distanceFromCenter);
        
        for(int i = 0; i < np; i++ ){
            System.out.print("Piece " + i + ": ");
            if( pista[i].isRecta() ) {
                System.out.println("Recta " + pista[i].length + ", " + pista[i].Switch);
            } else {
                System.out.println("Curva " + pista[i].radius + "," + pista[i].angle + ", " + pista[i].Switch);
            }
        }
        System.out.println("");
        calcularRutaoptima();
        calcularVelLim();
        
        for(int i = 0; i < np; i++)
            System.out.println("piece " + i + ": " + VM[i][0] + ", " + VM[i][1]);
    }
    
    Piece pista[] = null;
    Lanes carriles[] = null;
    int BEST[][], ncarriles, npieces;
    double DIST[][];
    boolean VIS[];
    
    void calcularRutaoptima(){
        int nc = carriles.length;
        int np = pista.length;
        for(int i = 0; i < nc; i++)
            for(int j = i+1; j < nc; j++)
                if( carriles[i].distanceFromCenter > carriles[j].distanceFromCenter ) swap(carriles[i],carriles[j]);
        
        VIS = new boolean[np];
        for(int i = 0; i < np; i++)
            VIS[i] = false;
        
        ncarriles = nc;
        npieces = np;
        BEST = new int[np][nc];
        DIST = new double[np][nc];
        for(int i = 0; i < nc; i++)
            DIST[0][i] = 0;
        
        for(int i = np-1; i >= 0; i-- ){
            for(int j = 0; j < nc; j++){
                DIST[i][j] = DIST[(i+1)%np][j];
                BEST[i][j] = 0;
                if( pista[i].isRecta() ) DIST[i][j] += pista[i].length;
                else DIST[i][j] += ((pista[i].radius - Math.signum(pista[i].angle)*carriles[j].distanceFromCenter)*Math.abs(pista[i].angle)*Math.PI)/180.0;
            }
            if( pista[i].Switch ){
                for(int j = 0; j < nc; j++){
                    double aux = 0;
                    if( pista[i].isRecta() ){
                        if( j > 0 ){
                            aux = DIST[(i+1)%np][j-1];
                            aux += Math.sqrt( Math.pow(pista[i].length,2) + Math.pow(carriles[j].distanceFromCenter - carriles[j-1].distanceFromCenter,2) );
                            DIST[i][j] = Math.min(DIST[i][j], aux);
                        }
                        if( j + 1 < nc ){
                            aux = DIST[(i+1)%np][j+1];
                            aux += Math.sqrt( Math.pow(pista[i].length,2) + Math.pow(carriles[j].distanceFromCenter - carriles[j+1].distanceFromCenter,2) );
                            DIST[i][j] = Math.min(DIST[i][j], aux);
                        }
                    } else {
                        if( j > 0 ){
                            aux = DIST[(i+1)%np][j-1];
                            double sign = Math.signum(pista[i].angle);
                            aux += getDistance(pista[i].radius - sign*carriles[j].distanceFromCenter, pista[i].radius - sign*carriles[j-1].distanceFromCenter, pista[i].angle);
                            DIST[i][j] = Math.min(DIST[i][j], aux);
                        }
                        if( j + 1 < nc ){
                            aux = DIST[(i+1)%np][j+1];
                            double sign = Math.signum(pista[i].angle);
                            aux += getDistance(pista[i].radius - sign*carriles[j].distanceFromCenter, pista[i].radius - sign*carriles[j+1].distanceFromCenter, pista[i].angle);
                            DIST[i][j] = Math.min(DIST[i][j], aux);
                        }
                    }
                }
            }
        }
        for(int i = 0; i < np; i++){
            for(int j = 0; j < nc; j++)
                BEST[i][j] = 0;
            if( pista[i].Switch ){
                for(int j = 0; j < nc; j++){
                    if( j > 0 && DIST[(i+1)%np][j-1] < DIST[(i+1)%np][j] ) BEST[i][j] = -1;
                    if( j + 1 < nc && DIST[(i+1)%np][j+1] < DIST[(i+1)%np][j+BEST[i][j]] ) BEST[i][j] = 1;
                }
            }
        }
    }
    
    void swap(Object a, Object b){
        Object aux = a;
        a = b;
        b = aux;
    }
    
    double getDistance(double r1, double r2, double alfa){
        alfa *= Math.PI/180.0;
        alfa = Math.abs(alfa);
        if( r1 > r2 ) {
            double aux = r1;
            r1 = r2;
            r2 = aux;
        }
        double alfa1 = Math.acos(r1/r2);
        if( alfa1 > alfa ) return Math.sqrt(r1*r1 + r2*r2 - r1*r2*Math.cos(alfa));
        double d = Math.sqrt( r2*r2 - r1*r1);
        return d + (alfa - alfa1)*r1;
    }
    
    double aL = 0.5;
    double K = 0.02;
    double VM[][];
    
    private void calcularVelLim(){
        
        VM = new double[npieces][ncarriles];
        for(int i = npieces-1; i >= 0; i--)
            for(int j = 0; j < ncarriles; j++){
                if( pista[i].isCurva() ) VM[i][j] = Math.sqrt(aL*(pista[i].radius - Math.signum(pista[i].angle)*carriles[j].distanceFromCenter));
                else {
                    int naux = (i+1)%npieces;
                    double dist = 0, x = pista[i].length, y = carriles[j+BEST[i][j]].distanceFromCenter - carriles[j].distanceFromCenter;
                    if( BEST[i][j] != 0 ) dist = Math.sqrt(x*x + y*y);
                    else dist = x;
                    VM[i][j] = VM[naux][j + BEST[i][j]] + K*dist;
                }
            }
        
        for(int i = npieces-1; i >= 0; i--)
            for(int j = 0; j < ncarriles; j++){
                if( pista[i].isCurva() ) VM[i][j] = Math.sqrt(aL*(pista[i].radius - Math.signum(pista[i].angle)*carriles[j].distanceFromCenter));
                else {
                    int naux = (i+1)%npieces;
                    double dist = 0, x = pista[i].length, y = carriles[j+BEST[i][j]].distanceFromCenter - carriles[j].distanceFromCenter;
                    if( BEST[i][j] != 0 ) dist = Math.sqrt(x*x + y*y);
                    else dist = x;
                    VM[i][j] = VM[naux][j + BEST[i][j]] + K*dist;
                }
            }
    }
}
